const router = require('express').Router()
const Users = require('./models/Users')

router.get('/usuarios', (req, res) => {
    res.json([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
})

router.post('/usuarios', (req, res) => {
    //req.body muestra los parametros que le mandas
    console.log(req.body)
    const usuarioNuevo = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        edad: req.body.edad,
        casado: req.body.casado,
    }
    return new Users(usuarioNuevo)
        .save()
        .then((doc) => {
            console.log('Usuario grabado en la base de datos', doc)
            res.json(doc)
        })
        .catch((error) => {
            const message = 'Error al guardar usuario en la DB'
            console.error(message, error)
            res.status(500).json({ message })
        })

    res.send('Creando el usuario, aguarde un momento')
}) 

router.put('/usuarios/:id', (req, res) => {
    res.send('Actualizando el usuario, aguarde un momento')
})

router.delete('/usuarios/:id', (req, res) => {
    res.send('Borrando el usuario, aguarde un momento')
}) 

router.get('/usuarios/:id', (req, res) => {
    const id = req.params.id
    res.send('Obteniendo perfil del usuario con el ID ' + id)
})

module.exports = router