const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const app = express()
const port = 3000
const mongoConfig = { useNewUrlParser: true }

mongoose.connect('mongodb://localhost:27017/test', mongoConfig);

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/Felicidades/:de/:para', (req, res) => {
    const de = req.params.de
    const para = req.params.para
    res.send('Hola ' + de + ' muchas felicidades. De: ' + para)
})

app.get('/animal/:anim/:sonido', (req, res) => {
    const anim = req.params.anim
    const sonido = req.params.sonido
    res.send('El ' + anim + ' hace ' + sonido)
})

const rutas = require('./rutas')
app.use('/', rutas)

app.listen(port, () => console.log(`Example app listen on port ${port}!`))
